<div align=center><img src="https://images.gitee.com/uploads/images/2021/0618/095342_4da2ec7b_2073279.png" /></div>


### 美狐美颜开源版（如果对你有用，请给个star！)

---

#### 联系我们

点击链接加入群聊【美狐开源学习交流群】：https://jq.qq.com/?_wv=1027&k=8b8WsWFE


![输入图片说明](https://images.gitee.com/uploads/images/2021/0617/092818_7eda5f96_9236797.png "美狐开源学习交流群群二维码.png")



技术客服qq：2862937527


#### 项目介绍
美狐美颜sdk，是以人脸识别技术为核心，提供专业级实时美颜、大眼瘦脸、美颜滤镜、动态贴纸等滤镜的移动端sdk，以打造多功能视频美颜软件为目标，充分满足客户在直播美颜、短视频美颜等众多音视频软件业务场景的美化需求。现推出开源版本iOS版，Android开源版本正在研发中敬请期待！

演示demo：<img src="https://images.gitee.com/uploads/images/2021/0611/133433_ea131003_2073279.png" width="400"/>

#### 接入说明
下载项目后 用Xcode打开 真机运行即可展示效果

#### 功能亮点

![输入图片说明](https://images.gitee.com/uploads/images/2021/0607/164425_6c9a87c5_2073279.png "meiyan01.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/085441_9fdfc91e_2073279.png "微信截图_20210609085406.png")

#### 版本对比
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/084021_bd43553c_2073279.png "555555(1).png")
#### 开源版使用须知

- 允许用于个人学习、教学案例
- 开源版不适合商用，商用请购买商业版
- 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负
#### 参考项目
GPUImage
[https://github.com/Bonway/BBGPUImage](http://)
gpurenderKit
[https://github.com/Dongdong1991/GPURenderKitDemo
](http://)


#### 商业合作
美狐sdk精简版、基础版、高级版拥有更加完善的功能、性能更高、更稳定，支持目前主流的云服务平台

如果您想使用功能更完善的美颜sdk，请联系我们 获取精简版、基础版、高级版

![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/084823_2c276b86_2073279.png "微信截图_20210609084741.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0609/084832_89c09f16_2073279.png "微信截图_20210609084632.png")



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

